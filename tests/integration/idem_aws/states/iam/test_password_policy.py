import copy
import time
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-password-policy-" + str(int(time.time())),
    "minimum_password_length": 8,
    "require_symbols": False,
    "require_numbers": False,
    "require_uppercase_characters": True,
    "require_lowercase_characters": True,
    "allow_users_to_change_password": False,
    "password_reuse_prevention": 1,
    "hard_expiry": False,
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup):
    global PARAMETER
    ctx["test"] = __test

    await hub.states.aws.iam.password_policy.absent(
        ctx,
        name="password-policy",
    )

    ret = await hub.states.aws.iam.password_policy.present(
        ctx,
        **PARAMETER,
    )

    assert ret["result"], ret["comment"]

    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.iam.password_policy",
                name=PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.iam.password_policy",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert_password_policy(hub, ctx, resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.iam.password_policy.describe(ctx)
    resource_id = "password-policy"
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.iam.password_policy.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(
        "aws.iam.password_policy.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_password_policy(hub, ctx, described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="modify_password_policy_attributes", depends=["describe"])
async def test_modify_attributes(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test

    new_parameter["minimum_password_length"] = 6
    new_parameter["require_uppercase_characters"] = False
    new_parameter["require_lowercase_characters"] = False
    new_parameter["allow_users_to_change_password"] = True
    new_parameter["password_reuse_prevention"] = None
    new_parameter["hard_expiry"] = False

    ret = await hub.states.aws.iam.password_policy.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.iam.password_policy",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.iam.password_policy",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert_password_policy(hub, ctx, ret["old_state"], PARAMETER)
    assert_password_policy(hub, ctx, ret["new_state"], new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="test_exec_get", depends=["modify_password_policy_attributes"]
)
async def test_exec_get(hub, ctx, __test):
    global PARAMETER
    ret = await hub.exec.aws.iam.password_policy.get(ctx=ctx, name=PARAMETER["name"])
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert_password_policy(hub, ctx, ret["ret"], PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["test_exec_get"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.iam.password_policy.absent(
        ctx,
        name=PARAMETER["name"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert_password_policy(hub, ctx, ret["old_state"], PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.iam.password_policy",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.iam.password_policy",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.iam.password_policy.absent(
        ctx,
        name=PARAMETER["name"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.iam.password_policy",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    ret = await hub.states.aws.iam.password_policy.absent(
        ctx,
        name="password-policy",
    )
    assert ret["result"], ret["comment"]


def assert_password_policy(hub, ctx, resource, parameters):
    assert parameters.get("minimum_password_length") == resource.get(
        "minimum_password_length"
    )
    assert parameters.get("require_symbols") == resource.get("require_symbols")
    assert parameters.get("require_numbers") == resource.get("require_numbers")
    assert parameters.get("require_uppercase_characters") == resource.get(
        "require_uppercase_characters"
    )
    assert parameters.get("require_lowercase_characters") == resource.get(
        "require_lowercase_characters"
    )
    assert parameters.get("allow_users_to_change_password") == resource.get(
        "allow_users_to_change_password"
    )
    assert parameters.get("password_reuse_prevention") == resource.get(
        "password_reuse_prevention"
    )
