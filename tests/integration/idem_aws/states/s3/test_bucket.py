import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_bucket(hub, ctx):
    # Create bucket
    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())
    tags = [{"Key": "Name", "Value": bucket_temp_name}]
    create_bucket_configuration = {"LocationConstraint": ctx.acct.region_name}
    object_ownership = "BucketOwnerEnforced"

    # Create s3 bucket with test flag "idem state --test"
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.s3.bucket.present(
        test_ctx,
        acl="private",
        name=bucket_temp_name,
        create_bucket_configuration=create_bucket_configuration,
        object_lock_enabled_for_bucket=True,
        object_ownership=object_ownership,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.s3.bucket '{bucket_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )
    assert bucket_temp_name == resource.get("name")
    assert create_bucket_configuration == resource.get("create_bucket_configuration")
    assert object_ownership == resource.get("object_ownership")
    assert "private" == resource.get("acl")
    assert resource.get("object_lock_enabled_for_bucket")

    # Create s3 bucket
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        create_bucket_configuration=create_bucket_configuration,
        object_lock_enabled_for_bucket=True,
        object_ownership=object_ownership,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Created aws.s3.bucket '{bucket_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    assert bucket_temp_name == resource.get("name")
    assert create_bucket_configuration == resource.get("create_bucket_configuration")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )
    # Localstack pro does not populate object_ownership in the output
    if not hub.tool.utils.is_running_localstack(ctx):
        assert object_ownership == resource.get("object_ownership")
    assert resource.get("object_lock_enabled_for_bucket")
    assert resource.get("full_control")

    new_tags = [{"Key": "Name", "Value": bucket_temp_name}]
    object_lock_configuration = {
        "ObjectLockEnabled": "Enabled",
        "Rule": {
            "DefaultRetention": {
                "Mode": "GOVERNANCE",
                "Days": 1,
            }
        },
    }

    # Update s3 bucket with test flag "idem state --test"
    ret = await hub.states.aws.s3.bucket.present(
        test_ctx,
        name=bucket_temp_name,
        resource_id=bucket_temp_name,
        create_bucket_configuration=create_bucket_configuration,
        object_lock_enabled_for_bucket=True,
        object_lock_configuration=object_lock_configuration,
        object_ownership=object_ownership,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert f"aws.s3.bucket '{bucket_temp_name}' already exists" in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        new_tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )
    assert bucket_temp_name == resource.get("name")
    assert create_bucket_configuration == resource.get("create_bucket_configuration")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert object_ownership == resource.get("object_ownership")
    assert resource.get("object_lock_enabled_for_bucket")
    assert resource.get("full_control")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert object_lock_configuration == resource.get("object_lock_configuration")

    # Update s3 bucket
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        resource_id=bucket_temp_name,
        create_bucket_configuration=create_bucket_configuration,
        object_lock_enabled_for_bucket=True,
        object_lock_configuration=object_lock_configuration,
        object_ownership=object_ownership,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert f"aws.s3.bucket '{bucket_temp_name}' already exists" in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        new_tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )
    assert bucket_temp_name == resource.get("name")
    assert create_bucket_configuration == resource.get("create_bucket_configuration")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert object_ownership == resource.get("object_ownership")
    assert resource.get("full_control")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert object_lock_configuration == resource.get("object_lock_configuration")

    # Describe s3 bucket
    describe_ret = await hub.states.aws.s3.bucket.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.s3.bucket.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get("aws.s3.bucket.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert described_resource_map.get("full_control")
    assert create_bucket_configuration == described_resource_map.get(
        "create_bucket_configuration"
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags,
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(
            described_resource_map.get("tags")
        ),
    )
    assert described_resource_map.get("object_lock_enabled_for_bucket")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert object_lock_configuration == described_resource_map.get(
            "object_lock_configuration"
        )

    # Delete s3 bucket with test flag "idem state --test"
    ret = await hub.states.aws.s3.bucket.absent(
        test_ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.s3.bucket '{bucket_temp_name}'" in ret["comment"]

    # Delete s3 bucket
    ret = await hub.states.aws.s3.bucket.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted aws.s3.bucket '{bucket_temp_name}'" in ret["comment"]
    resource = ret.get("old_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        new_tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )
    assert bucket_temp_name == resource.get("name")
    assert create_bucket_configuration == resource.get("create_bucket_configuration")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert object_ownership == resource.get("object_ownership")
    assert resource.get("full_control")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert object_lock_configuration == resource.get("object_lock_configuration")

    # Deleting s3 bucket again should be an no-op
    ret = await hub.states.aws.s3.bucket.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert f"aws.s3.bucket '{bucket_temp_name}' already absent" in ret["comment"]


@pytest.mark.asyncio
async def test_bucket_absent_with_none_resource_id(hub, ctx):
    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())
    # Delete s3 bucket with resource_id as None. Result in no-op.
    ret = await hub.states.aws.s3.bucket.absent(
        ctx, name=bucket_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.s3.bucket", name=bucket_temp_name
        )[0]
        in ret["comment"]
    )
