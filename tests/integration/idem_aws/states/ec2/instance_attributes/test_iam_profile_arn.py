import copy

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_update_iam_instance_profile_arn(
    hub, ctx, aws_ec2_instance, aws_iam_instance_profile, __test
):
    state = copy.copy(aws_ec2_instance)
    state["iam_instance_profile_arn"] = aws_iam_instance_profile["arn"]

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert (
            ret["changes"]["new"]["iam_instance_profile_arn"]
            == aws_iam_instance_profile["arn"]
        )
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]

    # Wait for the attaching to complete
    await hub.pop.loop.sleep(10)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_reset_iam_instance_profile_arn(hub, ctx, aws_ec2_instance, __test):
    state = copy.copy(aws_ec2_instance)
    state["iam_instance_profile_arn"] = None

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert not ret["changes"].get("new", {}).get("iam_instance_profile_arn")
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]
