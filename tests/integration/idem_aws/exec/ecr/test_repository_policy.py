import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR repositories")
async def test_get_invalid_repository(hub, ctx):
    fake_repository_name = str(uuid.uuid4())
    ret = await hub.exec.aws.ecr.repository_policy.get(
        ctx, name=fake_repository_name, resource_id=fake_repository_name
    )
    assert ret["result"]
    assert hub.tool.aws.comment_utils.get_empty_comment(
        resource_type="aws.ecr.repository_policy", name=fake_repository_name
    ) in str(ret["comment"])
    assert not ret["ret"]


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR repositories")
async def test_no_repository_policy_exists(hub, ctx, aws_test_ecr_repository):
    ret = await hub.exec.aws.ecr.repository_policy.get(
        ctx, name=aws_test_ecr_repository, resource_id=aws_test_ecr_repository
    )
    assert ret["result"], ret["comment"]
    assert hub.tool.aws.comment_utils.get_empty_comment(
        resource_type="aws.ecr.repository_policy", name=aws_test_ecr_repository
    ) in str(ret["comment"])
    assert not ret["ret"]


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR repositories")
async def test_repository_policy_exists(hub, ctx, aws_test_ecr_repository_with_policy):
    ret = await hub.exec.aws.ecr.repository_policy.get(
        ctx,
        name=aws_test_ecr_repository_with_policy,
        resource_id=aws_test_ecr_repository_with_policy,
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert aws_test_ecr_repository_with_policy == ret["ret"]["repository_name"]
    assert ret["ret"]["policy_text"]
